# Aufgabe 5: Dosenberechnung (12 Punkte)

Schreiben Sie nun ein Programm für Berechnungen, die für die Herstellung von Konservendosen aus einem Blechstück mit
* Umfang u (Umfang der Dose in Zentimetern) und
* Höhe h (Höhe der Dose in Zentimetern)
anfallen.
Ausgehend von den durch einen Nutzer eingegebenen Werten für u und h und unter Verwendung der Konstante \pi = 3.141592 die folgenden Werte berechnet und ausgibt:
* den Durchmesser des Dosenbodens: **$d_{boden} = \frac{u}{\pi}$**
* die Fläche des Dosenbodens: **$f_{boden} = \pi(\frac{d_{boden}{2})^2$**
* die Mantelfläche der Dose: **$f_{mantel} = uh$**
* die Gesamtfläche der Dose: **$f_{gesamt} = 2f_{boden} + f_{mantel}$**
* das Volumen der Dose: **$v = f_{boden}h$**
Erstellen Sie für die fünf oben genannten Berechnungen jeweils einzelne Routinen mit geeigneten Parametern und Rückgabewerten.
