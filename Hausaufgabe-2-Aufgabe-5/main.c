#include "stdafx.h"

#define PI 3.141592

// Gets a double value from console.
double consoleGetDouble(char *title)
{
  double d;
  printf(title);
  scanf_s("%lf", &d);
  return d;
}

double computeBottomDiameter(double circumference)
{
  return circumference / PI;
}

double computeBottomArea(double diameter)
{
  return pow(diameter / 2, 2) * PI;
}

double computeCoatArea(double circumference, double height)
{
  return circumference * height;
}

double computeFullArea(double bottom_area, double coat_area)
{
  return (bottom_area * 2) + coat_area;
}

double computeVolume(double bottom_area, double height)
{
  return bottom_area * height;
}

int main()
{
  double circumference  = consoleGetDouble("Den Umfang der Dose eingeben (in cm): ");
  double height         = consoleGetDouble("Die Hoehe der Dose eingeben (in cm):  ");
  double diameter       = computeBottomDiameter(circumference);
  double bottomArea     = computeBottomArea(diameter);
  double coatArea       = computeCoatArea(circumference, height);
  double fullArea       = computeFullArea(bottomArea, coatArea);
  double volume         = computeVolume(bottomArea, height);

  printf("----------------------------------------\n");
  printf("Umfang (in cm):           %10.2lf\n", circumference);
  printf("Hoehe (in cm):            %10.2lf\n", height);
  printf("Bodendurchmesser (in cm): %10.2lf\n", diameter);
  printf("Bodenflaeche (in cm^2):   %10.2lf\n", bottomArea);
  printf("Mantelflaeche (in cm^2):  %10.2lf\n", coatArea);
  printf("Gesamtflaeche (in cm^2):  %10.2lf\n", fullArea);
  printf("Volumen (in cm^3):        %10.2lf\n", volume);
  printf("Volumen (in l):           %10.2lf\n", volume / 1000);
  printf("----------------------------------------\n");

  _getch();

  return 0;
}
